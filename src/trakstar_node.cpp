#include <iostream>
#include <sstream>
#include <vector>
#include <ros/ros.h>
#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include "trakstar/PointATC3DG.hpp"
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/TransformStamped.h>


using namespace std;
using namespace trakstar;


int main(int argc, char* argv[]) {
  ros::init(argc, argv, "trakstar");
  ros::NodeHandle nh("~");
  
  bool publish_tf;
  nh.param<bool>("publish_tf", publish_tf, false);
  bool hemisphere_back;
  nh.param<bool>("hemisphere_back", hemisphere_back, false);
  bool range_72inch;
  nh.param<bool>("range_72inch", range_72inch, false);
  double rate;
  nh.param<double>("rate", rate, 255.0);
  
  ROS_INFO("Initializing TrakStar...");
  PointATC3DG bird;
  if (!bird) {
    ROS_ERROR("Can't initialize TrakStar!"); 
    return -1;
  }
  bird.setMeasurementRate(rate);
  ROS_INFO("... done.");
  
  bird.setSuddenOutputChangeLock(0);	
  int n_sensors = bird.getNumberOfSensors();
  ROS_INFO("Number of sensors: %d", n_sensors);
  
  for (int i = 0; i < n_sensors; ++i) {
    bird.setSensorQuaternion(i);
    if (hemisphere_back) {
      bird.setSensorHemisphere(i, HEMISPHERE_REAR);
    } else  {
      bird.setSensorHemisphere(i, HEMISPHERE_FRONT);
    }
  }
  
  if (range_72inch) {
    bird.setMaximumRange(true);
  }
  
  // check if transmitter attached
  int transmitter = bird.transmitterAttached();
  ROS_INFO("Transmitter attached: %d", transmitter);
  
  vector<ros::Publisher> pubs;
  for (int i = 0; i < n_sensors; ++i) {
    int sensor = bird.sensorAttached(i);
    ROS_INFO("Sensor %d attached: %d", i, sensor);
    
    stringstream pub_name;
    pub_name << "sensor" << i;
    pubs.push_back(
      nh.advertise<geometry_msgs::TransformStamped>(pub_name.str().c_str(), 100)
    );
  }
  
  tf::TransformBroadcaster* tf_broadcaster = NULL;
  if (publish_tf) {
    tf_broadcaster = new tf::TransformBroadcaster();
  }
    
  ros::Rate loop_rate(rate);
  double x, y, z;
  double* w = new double[4];
  std::string frame_names[4] = {"trakstar_sensor0", "trakstar_sensor1", "trakstar_sensor2", "trakstar_sensor3"};
  
  while (ros::ok()) {
    ros::spinOnce();
    
    //int ok = bird.ok();
    //ROS_INFO("OK: %d", ok);
    
    std::vector<geometry_msgs::TransformStamped> msgs(n_sensors);
    auto stamp = ros::Time::now();
    for (int i = 0; i < n_sensors; ++i) {
      bird.getCoordinatesQuaternion(i, x, y, z, w);
      //ROS_INFO("%f %f %f", x, y, z);
      tf::Vector3 pos(x, -y, z);
      tf::Quaternion q(w[1], -w[2], w[3], w[0]);
      tf::Matrix3x3 mat(q);
      tf::transformTFToMsg(tf::Transform(mat, pos), msgs[i].transform);
      msgs[i].header.stamp = stamp;
      msgs[i].header.frame_id = "trakstar_base";
      msgs[i].child_frame_id = frame_names[i];
      pubs[i].publish(msgs[i]);
    }
    
    if (tf_broadcaster)
    {
      tf_broadcaster->sendTransform(msgs);
    }
    
    loop_rate.sleep();
  }
  
  return 0;
}
